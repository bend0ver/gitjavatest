import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        System.out.println("Test - Andrei Ataman");
    }
}
class Ex1 {
    public static void main(String[] args) {
        byte x1 = 10;
        short x2 = x1;

        int y1 = 14;
        double y2 = y1;

        long z1 = 12312312L;
        int z2 = (int) z1;
    }
}
class Ex2 {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int x = kb.nextInt();
        String res = (x>0) ? "Positive" : "Negative";
        System.out.println(res);
    }
}
class Ex3 {
    public static void main(String[] args) {
        for(int i = 20; i < 41; i++)
            System.out.println(i);
    }
}
class Ex4 {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int x = kb.nextInt();
        if(x==0)
            System.out.println("Number is equals to 0");
        else if(x > 0)
            System.out.println("Number is positive");
        else
            System.out.println("Number is negative");
    }
}
class Ex5 {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int x = kb.nextInt();
        int y = kb.nextInt();
        int z = kb.nextInt();

        if(x == y && y == z && x == z)
            System.out.println("Numbers are equal");
        else
            System.out.println("Numbers are not equal");
    }
}
class Ex6 {
    public static void main(String[] args) {
        int[] vec = { 1, 2, 3 };
        int sum = 0;
        for(int x: vec)
            sum += x;
        System.out.println(sum);
    }
}
class Ex7 {
    public static void main(String[] args) {
        String str = "Eu astazi am prezentare la proiect final la step si e intraurea ca nu am fost informat";
        System.out.println(str.indexOf('m'));
        System.out.println(str.lastIndexOf('m'));
    }
}
class Ex8 {
    public static void main(String[] args) {
        String str = "    Programare Java    ";
        System.out.println(str.trim());
    }
}
class Produs {
    String name, cLoc;

    Produs(String name, String cLoc) {
        this.name = name;
        this.cLoc = cLoc;
    }
}
class Camion {
    static void start(Produs x, String loc) {
        System.out.println(x.name + " spre " + loc);
    }
}
class Ex9 {
    public static void main(String[] args) {
        String m1, m2 , m3;
        m1 = "Metro";
        m2 = "Nr1";
        m3 = "Linella";
        Produs cristinel = new Produs("Cristinel", "Deposit");
        Camion gigel = new Camion();
        gigel.start(cristinel, m1);
        gigel.start(cristinel, m2);
        gigel.start(cristinel, m3);
    }
}
class Ex10 {
    public static void main(String[] args) {
        int mL = 8;
        System.out.println(8*4);
    }
}
class Ex11 {
    public static void main(String[] args) {
        for(int i = 18; i < 33; i++)
            if(i%2==0)
                continue;
            else
                System.out.println(i);
    }
}
class Ex12 {
    public static void main(String[] args) {
        int x = 10;
        do {
            System.out.println(x);
        } while( x < 3);
        System.out.println("10 oricum va fi afisat odata");
    }
}
class Animal {
    static void eat() {
        System.out.println("Animal is eating");
    }
    static void sleep() {
        System.out.println("Animal is sleeping");
    }
}
class Dog extends Animal {
    static void bark(){
        System.out.println("Dog is barking");
    }
}
class Ex13 {
    public static void main(String[] args) {
        Animal x = new Animal();
        Dog y = new Dog();
        y.bark();
        y.sleep();
    }
}
class Vehicle {
    String name;
    static void exist() {
        System.out.println("Vehicle exists");
    }
}
class Ford extends Vehicle {
    String name;
    Ford(String name) {
        this.name = name;
    }
    void start() {
        System.out.println(name + " is starting up");
    }
    String getName() { return name; };
}
class BMW extends  Vehicle {
    String name;
    BMW(String name) {
        this.name = name;
    }
    void start() {
        System.out.println(name + " is starting up");
    }
    String getName() { return name; };
}
class Ex14 {
    public static void main(String[] args) {
        Vehicle x = new Vehicle();
        x.exist();

        Ford y = new Ford("Ford");
        y.start();
        y.getName();

        BMW z = new BMW("BWM");
        z.start();
        z.getName();
    }
}

class Bank {
	double sum;
	int months;
	int tax = 7;
	Bank(double sum, int months) {
		this.sum = sum;
		this.months = months;
	}
	void tax() {
		for(int i = 0; i < months - 1; i++)
			tax += 7;
		System.out.println(tax);
	}
	void retTotal() {
		double ret = (sum/100)*tax;
		System.out.println(ret);
	}
}

class Ex15 {
	public static void main(String[] args) {
		Bank x = new Bank(1250.0F, 3);
		x.tax();
		x.retTotal();
	}
}
